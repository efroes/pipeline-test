FROM bash:4.4

COPY . /ops-platform-users
RUN rm -rf /ops-platform-users/onboard/*-env.sh 
RUN rm -rf /ops-platform-users/onboard/README.md 

RUN apk add --update --no-cache jq unzip jq curl python2 

RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
RUN unzip awscli-bundle.zip && ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

WORKDIR /ops-platform-users/onboard

RUN chmod -R 777 /ops-platform-users/onboard

CMD ["bash", "onboard-customer.sh"]
