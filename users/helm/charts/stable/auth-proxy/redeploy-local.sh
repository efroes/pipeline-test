#!/usr/bin/env sh

APP=auth-proxy
helm uninstall $APP -n users
helm upgrade --install $APP . -n users --debug
