#!/bin/bash
# How to use
# ./redeploy.sh staging or ./redeploy.sh testing

BASEDIR=$(dirname "$0")
source $BASEDIR/../../../scripts/check_args.sh $@

APP=auth-proxy
helm upgrade --install $APP $BASEDIR -n users -f $STACKS_PATH/users/$APP/$ENVIRONMENT/values.yaml
