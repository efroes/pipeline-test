#!/bin/bash

# comment: Check if command exists
# syntax:
# checkCommand command1 command2 command3
#
# return: 0 is correct or code error
#
# Example:
# checkCommand git kubectl helm

commands="$*"

for command in $commands ; do
    if ! type "$command" > /dev/null 2>&1; then
        echo "[ERROR] Command '$command' not found."
        exit 4
    fi
done

# Validate AWS Cli Version
AWS_CLI_VERSION=$(aws --version 2>&1 | grep -Eo "aws-cli/[0-9.]+" | grep -Eo "[0-9.]+") 
AWS_MAJOR_VERSION=$(echo $AWS_CLI_VERSION | cut -d. -f1)
AWS_MINOR_VERSION=$(echo $AWS_CLI_VERSION | cut -d. -f2)

if [ ! $AWS_MAJOR_VERSION -eq 1 ]; then
   echo Detected aws-cli version is $AWS_CLI_VERSION, but version 1.x is Required
   exit 1
fi   

if [ $AWS_MINOR_VERSION -lt 19 ]; then
   echo Detected aws-cli version is $AWS_CLI_VERSION, but version 1.19+ is Required
   exit 1
fi   