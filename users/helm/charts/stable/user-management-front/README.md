# User Management Front Chart

Configure the Kubernetes resources for User Management Front service.

## TL;DR

```sh
# From Helm Chart source
helm upgrade --install user-management-front --namespace=users ./user-management-front/
```

or...

```sh
# Using the Helm Repo
helm repo add users https://storage.googleapis.com/sensedia-helm-charts/user-management-front/stable
helm upgrade --install user-management-front --namespace=users users/user-management-front
```
