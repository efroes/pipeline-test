#!/bin/bash
set -e

#create customer

CUSTOMER_ID="$1"
CUSTOMER_ALIAS="$2"

USER_POOL_ID="$(cat user_pool_output.json | jq -r '.UserPool.Id')"
USER_POOL_CLIENT_ID="$(cat user_pool_client_output.json | jq -r '.UserPoolClient.ClientId')"

curl -s --location --request POST "${USER_MGMT_ALB}/user-management/v1/customers" \
--header 'Content-Type: application/json' \
--data-raw "$(cat <<-EOF
{
    "id": "${CUSTOMER_ID}",
    "alias": "${CUSTOMER_ALIAS}",
    "userPool": {
        "id": "${USER_POOL_ID}",
        "poolName": "sensedia-${CUSTOMER_ID}",
        "region": "us-east-1",
        "clientId": "${USER_POOL_CLIENT_ID}"
    }
}
EOF
)"

echo "Customer ${CUSTOMER_ID}/${CUSTOMER_ALIAS} has been created."