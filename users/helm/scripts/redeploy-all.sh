#!/bin/bash

source check_args.sh $@
scripts=$(find ../charts/stable -name redeploy.sh)
for f in $scripts; do 
  echo Running $f
  bash "$f" $@
  echo
done
