# Terraform backend for remote state
terraform {
  backend "s3" {
    encrypt                 = true
    bucket                  = "terraform-remote-state-sensedia-dev"
    dynamodb_table          = "terraform-state-lock-dynamo-sensedia-dev"
    region                  = "us-east-1"
    workspace_key_prefix    = "users"
    key                     = "platform-users-tests/terraform.tfstate"
    profile                 = "developers-dev"
    shared_credentials_file = "~/.aws/credentials"
  }
}