#!/bin/bash
#------------------------
# Sensedia
# Authors: Aecio Pires
# Date: 13 jan 2020
#
# Objective: Send charts for Helm Repo in AWS and GCP
#
#--------------------- REQUISITES --------------------#
#  1) You need to create a key SSH in Git repository of Helm Charts and add a new credential of type SSH 
#    in Jenkins with the IDs defined in the variable: $CREDENTIAL_JENKINS_GIT.
#
#  2) Create a credential in AWS with permission access EKS and bucket S3 and add a new 
#    credential of type AWS (require install plugin aws-credentials) and define in variable: $AWS_CREDS
#
#  3) Create a bucket AWS S3 (see example and documentation in:
#    https://sensedia.atlassian.net/wiki/spaces/CLARK/pages/444203009/Sensedia+Helm+-+Guidelines )
#
#  4) Define region same EKS will use. Example: us-east-1 (Virginia)
#    https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html
#
#  5) Define profile to use services in to AWS. Example: sensedia-dev
#
#  6) Create a credential in GCP with permission access GCS and bucket GCS and add a new 
#    credential of type GCS (require install Gcloud-SDK) and define in variable: $GCP_CREDENTIALS_ID
#
#  7) Create a bucket GCP GCS (see example and documentation in:
#    https://sensedia.atlassian.net/wiki/spaces/CLARK/pages/444203009/Sensedia+Helm+-+Guidelines )
#
#  8) Define region same GCS will use. Example: US (Multi-Regional in USA)
#    https://cloud.google.com/storage/docs/locations
#
#  9) Configure integration with Email service and Slack channel of Sensedia for send notifications.
#
#------------------------

#------------------------
# Local Functions

#--------------------------------------------------------
# comment: Print usage help.
# sintax: usage
#
function usage() {

echo "Script to send Helm charts to AWS and GCP."
echo "Usage:"
echo " "
echo "$0 WORKSPACE_DIR URL_BUCKET_S3 URL_BUCKET_GCS SEND_ALL_CHARTS"
exit 3
}

#--------------------------------------------------------
# comment: Send charts for Helm Repo in AWS-S3
# sintax: send_charts_s3 $chart_name $chart_version $workspace_dir $bucket_s3
#
function send_charts_s3() {

#------------------------
# Variables of function

local CHART_NAME="$1"
local CHART_VERSION="$2"
local WORKSPACE_DIR="$3"
local URL_BUCKET_S3=$4
local URL_BUCKET_S3_CHARTS_STABLE="$URL_BUCKET_S3/charts/stable/"
local URL_BUCKET_S3_CHARTS_DEVELOP="$URL_BUCKET_S3/charts/develop/"
local aux_name=$(echo $URL_BUCKET_S3 | cut -d "/" -f3)
local URL_PUBLIC_S3="http://$aux_name.s3.amazonaws.com"
local URL_PUBLIC_S3_CHARTS_STABLE="$URL_PUBLIC_S3/charts/stable/"
local URL_PUBLIC_S3_CHARTS_DEVELOP="$URL_PUBLIC_S3/charts/develop/"
local PACKAGE_NAME=$CHART_NAME-$CHART_VERSION
local PACKAGE_NAME_ZIP=$PACKAGE_NAME.tgz
#------------------------


    # Remove old files
    rm $WORKSPACE_DIR/index.yaml_remote $WORKSPACE_DIR/index.yaml > /dev/null 2>&1

    cd $WORKSPACE_DIR
    echo "[INFO] Downloading index.yaml existing in remote bucket"
    if aws s3 cp "$URL_BUCKET_S3/index.yaml" "$WORKSPACE_DIR/index.yaml_remote"; then
        echo "[INFO] Check if '$CHART_NAME'-'$CHART_VERSION' chart exists in remote repo in AWS-S3."
        # Check if chart exists in remote repo in AWS-S3
        VERSION_REMOTE=$(cat $WORKSPACE_DIR/index.yaml_remote | grep -A1 -w "$URL_PUBLIC_S3_CHARTS_STABLE$CHART_NAME-$CHART_VERSION.tgz" | grep version | tr -d ' ' | cut -d':' -f2)

        if $DEBUG ; then
            echo "[DEBUG] INIT -------------"
            echo "Checking if exists '$CHART_NAME' chart with version '$CHART_VERSION' in remote repo in AWS-S3."
            echo "COMMAND_S3 => cat $WORKSPACE_DIR/index.yaml_remote | grep -A1 -w '$URL_PUBLIC_S3_CHARTS_STABLE$CHART_NAME-$CHART_VERSION.tgz' | grep version | tr -d ' ' | cut -d':' -f2"
            echo "Version desired => '$CHART_VERSION'"
            echo "Version remote  => '$VERSION_REMOTE'"
            echo "[DEBUG] END"
        fi

        if [ "$CHART_VERSION" == "$VERSION_REMOTE" ]; then
            echo "+++++++++++++++++++++++++++++++++++++"
            echo "[WARNING] '$CHART_NAME'-'$CHART_VERSION' chart exists in remote repo '$HELM_REPO_LOCALLY_S3'.";
            echo "Was there a change in the version of the Chart.yaml file of $CHART_NAME?";
            echo "+++++++++++++++++++++++++++++++++++++"
        else
            # Generate new helm chart
            generate_helm_chart $CHART_NAME $CHART_VERSION $WORKSPACE_DIR
            helm repo index --url $URL_PUBLIC_S3_CHARTS_STABLE --merge $WORKSPACE_DIR/index.yaml_remote $WORKSPACE_DIR
            aws s3 cp $WORKSPACE_DIR/$PACKAGE_NAME_ZIP $URL_BUCKET_S3_CHARTS_STABLE
            aws s3 cp $WORKSPACE_DIR/index.yaml $URL_BUCKET_S3
        fi
    else
        # Generate new helm chart
        generate_helm_chart $CHART_NAME $CHART_VERSION $WORKSPACE_DIR

        echo "[WARNING] Not exists index.yaml file in remote bucket. Generating new index.yaml."
        helm repo index $WORKSPACE_DIR --url $URL_PUBLIC_S3_CHARTS_STABLE
        aws s3 cp $WORKSPACE_DIR/$PACKAGE_NAME_ZIP $URL_BUCKET_S3_CHARTS_STABLE
        aws s3 cp $WORKSPACE_DIR/index.yaml $URL_BUCKET_S3
    fi

    #if $DEBUG ; then
    #    echo "[DEBUG] INIT -------------"
    #    echo "List content of $WORKSPACE_DIR/index.yaml file for S3."
    #    ls -l $WORKSPACE_DIR/index.yaml
    #    cat $WORKSPACE_DIR/index.yaml
    #    echo "[DEBUG] END"
    #fi

}


#--------------------------------------------------------
# comment: Send charts for Helm Repo in GCP-GCS.
# sintax: send_charts_gcs $chart_name $chart_version $workspace_dir $bucket_gcs
#
function send_charts_gcs() {

#------------------------
# Variables of function

local CHART_NAME="$1"
local CHART_VERSION="$2"
local WORKSPACE_DIR="$3"
local URL_BUCKET_GCS=$4
local URL_BUCKET_GCS_CHARTS_STABLE="$URL_BUCKET_GCS/charts/stable/"
local URL_BUCKET_GCS_CHARTS_DEVELOP="$URL_BUCKET_GCS/charts/develop/"
local aux_name=$(echo $URL_BUCKET_GCS | cut -d "/" -f3)
local URL_PUBLIC_GCS="https://$aux_name.storage.googleapis.com"
local URL_PUBLIC_GCS_CHARTS_STABLE="$URL_PUBLIC_GCS/charts/stable/"
local URL_PUBLIC_GCS_CHARTS_DEVELOP="$URL_PUBLIC_GCS/charts/develop/"
local PACKAGE_NAME=$CHART_NAME-$CHART_VERSION
local PACKAGE_NAME_ZIP=$PACKAGE_NAME.tgz
#------------------------

    # Remove old files
    rm $WORKSPACE_DIR/index.yaml_remote $WORKSPACE_DIR/index.yaml > /dev/null 2>&1

    cd $WORKSPACE_DIR
    echo "[INFO] Downloading index.yaml existing in remote bucket"
    if gsutil cp "$URL_BUCKET_GCS/index.yaml" "$WORKSPACE_DIR/index.yaml_remote"; then
        echo "[INFO] Check if '$CHART_NAME'-'$CHART_VERSION' chart exists in remote repo in GCP-GCS."
        # Check if chart exists in remote repo in GCP-GCS
        VERSION_REMOTE=$(cat $WORKSPACE_DIR/index.yaml_remote | grep -A1 -w "$URL_PUBLIC_GCS_CHARTS_STABLE$CHART_NAME-$CHART_VERSION.tgz" | grep version | tr -d ' ' | cut -d':' -f2)

        if $DEBUG ; then
            echo "[DEBUG] INIT -------------"
            echo "Checking if exists '$CHART_NAME' chart with version '$CHART_VERSION' in remote repo in GCP-GCS."
            echo "COMMAND_GCS => cat $WORKSPACE_DIR/index.yaml_remote | grep -A1 -w '$URL_PUBLIC_GCS_CHARTS_STABLE$CHART_NAME-$CHART_VERSION.tgz' | grep version | tr -d ' ' | cut -d':' -f2"
            echo "Version desired => '$CHART_VERSION'"
            echo "Version remote  => '$VERSION_REMOTE'"
            echo "[DEBUG] END"
        fi

        if [ "$CHART_VERSION" == "$VERSION_REMOTE" ]; then
            echo "+++++++++++++++++++++++++++++++++++++"
            echo "[WARNING] '$CHART_NAME'-'$CHART_VERSION' chart exists in remote repo '$HELM_REPO_LOCALLY_GCS'.";
            echo "Was there a change in the version of the Chart.yaml file of $CHART_NAME?";
            echo "+++++++++++++++++++++++++++++++++++++"
        else
            # Generate new helm chart
            generate_helm_chart $CHART_NAME $CHART_VERSION $WORKSPACE_DIR
            helm repo index --url $URL_PUBLIC_GCS_CHARTS_STABLE --merge $WORKSPACE_DIR/index.yaml_remote $WORKSPACE_DIR
            gsutil cp $WORKSPACE_DIR/$PACKAGE_NAME_ZIP $URL_BUCKET_GCS_CHARTS_STABLE
            gsutil cp $WORKSPACE_DIR/index.yaml $URL_BUCKET_GCS
        fi
    else
        # Generate new helm chart
        generate_helm_chart $CHART_NAME $CHART_VERSION $WORKSPACE_DIR

        echo "[WARNING] Not exists index.yaml file in remote bucket. Generating new index.yaml."
        helm repo index $WORKSPACE_DIR --url $URL_PUBLIC_GCS_CHARTS_STABLE
        gsutil cp $WORKSPACE_DIR/$PACKAGE_NAME_ZIP $URL_BUCKET_GCS_CHARTS_STABLE
        gsutil cp $WORKSPACE_DIR/index.yaml $URL_BUCKET_GCS
    fi

    #if $DEBUG ; then
    #    echo "[DEBUG] INIT -------------"
    #    echo "List content of $WORKSPACE_DIR/index.yaml file for GCS."
    #    ls -l $WORKSPACE_DIR/index.yaml
    #    cat $WORKSPACE_DIR/index.yaml
    #    echo "[DEBUG] END"
    #fi

}

#--------------------------------------------------------
# comment: Generate Helm charts
# sintax: generate_helm_chart $chart_name $chart_version $workspace_dir
#
function generate_helm_chart() {

#------------------------
# Variables of function

local CHART_NAME="$1"
local CHART_VERSION="$2"
local WORKSPACE_DIR="$3"
local PACKAGE_NAME=$CHART_NAME-$CHART_VERSION
local PACKAGE_NAME_ZIP=$PACKAGE_NAME.tgz

#------------------------

# Check semantic version
checkSemanticVersion $CHART_VERSION
if [ "$?" -ne "0" ]; then
    echo "[ERROR] Failed to validate chart version '$CHART_VERSION' in branch name for chart '$CHART_NAME'."
    exit 6
fi

# Check if existis chart stable
if [ -d $WORKSPACE_DIR/charts/stable/$CHART_NAME ]; then
    cd $WORKSPACE_DIR

    echo "[INFO] Generate new helm chart for '$CHART_NAME'-'$CHART_VERSION'...";
    # Upgrade chart version
    if [ ! -f charts/stable/$CHART_NAME/Chart.yaml ]; then
        echo "[ERROR] Not found '$WORKSPACE_DIR/charts/stable/$CHART_NAME/Chart.yaml' file."
        exit 8
    else
        sed "s~version:\(.*\)~version: $CHART_VERSION~" $WORKSPACE_DIR/charts/stable/$CHART_NAME/Chart.yaml > /tmp/aux.$$
        mv /tmp/aux.$$ $WORKSPACE_DIR/charts/stable/$CHART_NAME/Chart.yaml
    fi

    # Execute the lint of Helm chart
    helmLint $WORKSPACE_DIR/charts/stable/$CHART_NAME
    if [ "$?" -ne "0" ]; then
        echo "[ERROR] Failed to chart lint '$WORKSPACE_DIR/charts/stable/$CHART_NAME'."
        exit 9
    fi

    if $DEBUG ; then
        echo "[DEBUG] INIT -------------"
        echo "Current directory before generate package of chart."
        pwd
        echo "..."
        echo "..."
        echo "..."
        echo "[DEBUG] END"
    fi

    # Generate chart package
    helm package $WORKSPACE_DIR/charts/stable/$CHART_NAME

    if $DEBUG ; then
        echo "[DEBUG] INIT -------------"
        echo "List files in directory '$WORKSPACE_DIR/'."
        ls -l $WORKSPACE_DIR/
        echo "..."
        echo "..."
        echo "..."
        echo "List files in directory '$WORKSPACE_DIR/charts/stable/$CHART_NAME'."
        ls -l $WORKSPACE_DIR/charts/stable/$CHART_NAME
        echo "[DEBUG] END"
    fi
else
    echo "[ERROR] Not exitis '$CHART_NAME' chart in directory $WORKSPACE_DIR/charts/stable."
    exit 11
fi

}

#------------------------



#------------------------
# Variables

workspace_dir=$1
bucket_s3=$2
bucket_gcs=$3
# If false send one chart once branch (default)
# If true send all chart without any verification
SEND_ALL_CHARTS=$4


PROGPATHNAME=$0
PROGFILENAME=$(basename $PROGPATHNAME)
PROGDIRNAME=$(dirname $PROGPATHNAME)

DEBUG=true
# To don't force send chart uncomment the follow line
FORCE_SEND_CHART=" "
# To Force send chart uncomment the follow line
#FORCE_SEND_CHART="--force"

HELM_REPO_LOCALLY_S3="sensedia-s3"
HELM_REPO_LOCALLY_GCS="sensedia-gcs"

#------------------------

# Load script with our libs and defaults functions
[ -x $PROGDIRNAME/lib.sh ] && . $PROGDIRNAME/lib.sh

# Testing if variable is empty
checkVariable workspace_dir "$workspace_dir"
checkVariable bucket_s3 "$bucket_s3"
checkVariable bucket_gcs "$bucket_gcs"
checkVariable SEND_ALL_CHARTS "$SEND_ALL_CHARTS"


# Testing if commands existis
checkCommand git helm aws gcloud gsutil

# Variables create in run time
cd $workspace_dir

if $SEND_ALL_CHARTS ; then
    # Send all chart without any verification
    # Don't use in conditions normal
    for chart_name in `ls $workspace_dir/charts/stable`; do
        # Get actual version of helm chart
        chart_version=$(cat $workspace_dir/charts/stable/$chart_name/Chart.yaml | grep -w '^version:' | cut -d':' -f2 | tr -d ' ')

        # Send helm chart to remote repo
        send_charts_s3 $chart_name $chart_version $workspace_dir $bucket_s3
        send_charts_gcs $chart_name $chart_version $workspace_dir $bucket_gcs
    done
else
    # Get name and version of Helm Chart from the branch name
    chart_name=$(git log --oneline --merges --grep='pull request' -n1 | cut -d' ' -f4 | cut -d'_' -f1)
    chart_version=$(git log --oneline --merges --grep='pull request' -n1 | cut -d' ' -f4 | cut -d'_' -f2)

    # Update version of helm chart and send to remote repo
    send_charts_s3 $chart_name $chart_version $workspace_dir $bucket_s3
    send_charts_gcs $chart_name $chart_version $workspace_dir $bucket_gcs
fi

echo "Happy Helming!!! =)"
