# Provider config
variable "credentials_file" {
  default = "~/.aws/credentials"
}

variable "profile" {}
variable "region" {}

variable "environment" {}

variable "cognito_public_info_policy_name" {
  default = "CognitoUserPoolAutTestingPolicy"
}
variable "cognito_public_info_role_name" {
  default = "CognitoUserPoolAutTestingRole"
}
variable "cognito_public_info_identity_pool_name" {
  default = "Cognito Identity Pool Aut Testing"
}

variable "tags" {
  type    = map(string)
  default = {}
}