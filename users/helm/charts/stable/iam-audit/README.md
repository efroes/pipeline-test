# User Management Chart

Configure the Kubernetes resources for User Management service.

## TL;DR

```sh
# From Helm Chart source
helm upgrade --install iam-audit --namespace=users ./iam-audit/
```

or...

```sh
# Using the Helm Repo
helm repo add users https://storage.googleapis.com/sensedia-helm-charts/iam-audit/stable
helm upgrade --install iam-audit --namespace=users users/iam-audit
```
