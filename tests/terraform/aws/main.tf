data "local_file" "workspace_check" {
  count    = var.environment == terraform.workspace ? 0 : 1
  filename = "ERROR: Workspace does not match given environment name!"
}
