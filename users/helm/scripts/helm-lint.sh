#!/bin/bash

BASEDIR=$(dirname "$0")

set -e

dirs=$(ls $BASEDIR/../charts/stable)
for d in $dirs; do 
  echo $d
  helm lint $BASEDIR/../charts/stable/$d
done
