#!/bin/bash

docker run --rm -it amazon/aws-cli ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 660412058498.dkr.ecr.us-east-1.amazonaws.com

docker build -t ops-platform-users-onboard .

docker tag ops-platform-users-onboard:latest 660412058498.dkr.ecr.us-east-1.amazonaws.com/ops-platform-users-onboard:latest

docker push 660412058498.dkr.ecr.us-east-1.amazonaws.com/ops-platform-users-onboard:latest