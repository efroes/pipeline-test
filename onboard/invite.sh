#!/bin/bash
set -e

bash check-commands.sh curl

CUSTOMER_ID="$1"
ROOT_USER="$2"
ENVIRONMENT="$3"

if [ ! -f "$ENVIRONMENT-env.sh" ]; then
    echo "$ENVIRONMENT-env.sh file does not exist.";
    exit 1;
fi

source $ENVIRONMENT-env.sh

# create root user
curl -s --location --request POST "${USER_MGMT_ALB}/user-management/v1/users" \
--header 'customerId: '${CUSTOMER_ID}'' \
--header 'username: '${ROOT_USER}'' \
--header 'Content-Type: application/json' \
--data-raw "$(cat <<-EOF
{
    "username": "${ROOT_USER}",
    "email": "${ROOT_USER}",
    "confirmEmail": "${ROOT_USER}",
    "tags": [
        "SUPER ADMIN"
    ],
    "policies": [
        {
            "id": "6397f37a-42b0-46ec-840f-a5345672c8ae"
        }
    ]
}
EOF
)"

echo "User ${ROOT_USER} has been invited."