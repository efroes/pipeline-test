# Onboarding Process

The onboarding process is responsible for creating the UserPool at Cognito, configurations on DynamoDB and the invite user root.

## Requirements

1. Install [aws cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) `aws-cli/2.1.27` or higher
2. Install [jq](https://stedolan.github.io/jq/download/) `jq/1.5` or higher
3. Install [curl](https://curl.se/download.html) `curl/7.68.0` or higher

## Run

```bash
bash onboard-customer.sh [customer_id] [customer_alias] [environment]
bash invite.sh [customer_id] [root_account_email] [environment]
```

## Variables

|Name|Description|
|---|---|
|curtomer_id|Ten characters length sequence wich defines a TenantId|
|customer_alias|The customer alias to be assined as the DNS ie: (__myalias__.signin.platform.sensedia.com)|
|root_account_email|The first user's e-mail, which will have unlimited access to the IAM module. It's up to this user to invite other users.|
