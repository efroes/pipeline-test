#!/usr/bin/env sh

APP=user-management
helm delete $APP -n users
helm upgrade --install $APP . -n users
