#!/bin/bash
set -e

CUSTOMER_ID="$1"
CUSTOMER_ALIAS="$2"

USER_POOL_ID="$(cat user_pool_output.json | jq -r '.UserPool.Id')"
USER_POOL_CLIENT_ID="$(cat user_pool_client_output.json | jq -r '.UserPoolClient.ClientId')"

JWT_SIGNATURE=$(curl -XGET -s https://cognito-idp.us-east-1.amazonaws.com/${USER_POOL_ID}/.well-known/jwks.json | sed 's/"/\\"/g')

aws dynamodb put-item \
    --table-name "${CUSTOMER_CONFIG_TABLE}" \
    --item "$(cat <<-EOF
{
    "CustomerId": {
        "S": "${CUSTOMER_ID}"
    },
    "Alias": {
        "S": "${CUSTOMER_ALIAS}"
    },
    "JWTSignature": {
        "S": "${JWT_SIGNATURE}"
    },
    "Region": {
        "S": "us-east-1"
    },
    "UserPoolId": {
        "S": "${USER_POOL_ID}"
    },
    "ClientId": {
        "S": "${USER_POOL_CLIENT_ID}"
    }
}
EOF
)"

echo "The customer has been configured at DynamoDB."