#!/bin/bash

if [ -z $STACKS_PATH ]; then
    echo "STACKS_PATH is not defined as env var. Set it to the path to the user management stacks directory (e.g.: ~/work/stacks/helm/services/user-management)"
    exit 1;
fi

echo "Using $STACKS_PATH as STACKS_PATH"

ENVIRONMENT=$1

if [ -z $ENVIRONMENT ]; then
    ENVIRONMENT="testing"
fi

echo "Using $ENVIRONMENT as ENVIRONMENT"

if [ ! -d $STACKS_PATH ]; then
    echo "$STACKS_PATH does not exist. Exiting...";
    exit 1;
fi
