#!/bin/bash

detect_accented_characters() {
  PARAM=$1
  ERROR_MSG=$2

  #converting from UTF-8 to ASCII to remove accented characters
  ASCII=$(echo $PARAM | iconv -f UTF8 -t ASCII//TRANSLIT)

  if [[ "$ASCII" != "$PARAM" ]]; then
    echo "$ERROR_MSG"
    exit 1
  fi
}

validate_parameter() {
  PARAM=$1
  REGEX=$2
  ERROR_MSG=$3

  detect_accented_characters "$PARAM" "$ERROR_MSG"

  if ! [[ $PARAM =~ $REGEX ]];then
    echo "$ERROR_MSG"
    exit 1
  fi
}

case $2 in
 "--customer-alias")
      CUSTOMER_ALIAS=$1
      if [[ ${#CUSTOMER_ALIAS} -gt 20 || ${#CUSTOMER_ALIAS} -lt 2 ]]; then
        echo "Customer alias must be between 2-20 characters long"
        exit 1
      fi

      REGEX='^[0-9a-z]*(\-?[0-9a-z]+)*[a-z]+[0-9a-z]*$'
      ERROR_MSG="Invalid customer alias '$CUSTOMER_ALIAS'. Customer alias must contain only lower case alphanumeric characters and hyphens (non-repeating); must not start or end with hyphens; must contain at least a letter"
      validate_parameter "$CUSTOMER_ALIAS" "$REGEX" "$ERROR_MSG"
      ;;
  *)
      echo "Invalid flag '$2'"
      exit 1
      ;;
esac




