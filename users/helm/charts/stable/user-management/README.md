# User Management Chart

Configure the Kubernetes resources for User Management service.

## TL;DR

```sh
# From Helm Chart source
helm upgrade --install user-management --namespace=users ./user-management/
```

or...

```sh
# Using the Helm Repo
helm repo add users https://storage.googleapis.com/sensedia-helm-charts/user-management/stable
helm upgrade --install user-management --namespace=users users/user-management
```
