#!/bin/sh

export EMAIL_ARN="arn:aws:ses:us-east-1:660412058498:identity/noreply@sensedia.com"
export LAMBDA_ARN_THIRD_PARTY_SIGNUP="arn:aws:lambda:us-east-1:660412058498:function:lambda-third-party-signup-testing"
export USER_MGMT_ALB="http://e1a0fd59-users-usermanagem-fc17-1116637109.us-east-1.elb.amazonaws.com"
export CUSTOMER_CONFIG_TABLE="CustomerConfigTesting"