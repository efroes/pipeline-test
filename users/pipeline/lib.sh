#!/bin/bash

#------------------------------------------------------------
# Default function to general use
# Do nothing. Just define functions.
# Load with ". ./lib.sh"
#------------------------------------------------------------


#--------------------------------------------------------
# comment: Retries a command on failure.
# sintax:
# retry number_attempts command
#
# Example:
# retry 5 ls -ltr foo
#
# Font: http://fahdshariff.blogspot.com/2014/02/retrying-commands-in-shell-scripts.html
function retry() {

# Creates a read-only local variable of integer type
local -r -i max_attempts="$1"; shift
# Create a read-only local variable
local -r command="$@"
# Create a local variable of integer type 
local -i attempt_num=1
local -i time_seconds=30


until $command; do
    if (( attempt_num == max_attempts )); then
        echo "[ERROR] Attempt $attempt_num failed and there are no more attempts left!"
        return 1
    else
        echo "[WARNING] Attempt $attempt_num failed! Trying again in $time_seconds seconds..."
        sleep $time_seconds
    fi
done

}


#--------------------------------------------------------
# comment: Test that kubectl can correctly communication with the Kubernetes API
# sintax:
# testAccessKubernetes
#
# return: 0 is correct or code error
#
# Example:
# testAccessKubernetes
#
# Font: https://github.com/judexzhu/Jenkins-Pipeline-CI-CD-with-Helm-on-Kubernetes/blob/master/Jenkinsfile
function testAccessKubernetes() {

echo "Test that kubectl can correctly communication with the Kubernetes API"
kubectl cluster-info
kubectl get nodes

return $?
}


#--------------------------------------------------------
# comment: Execute the lint of Helm chart
# sintax:
# helmLint chartdir
#
# return: 0 is correct or code error
#
# Example:
# helmLint stable/user-management
#
# Font: https://github.com/judexzhu/Jenkins-Pipeline-CI-CD-with-Helm-on-Kubernetes/blob/master/Jenkinsfile
function helmLint() {

local chart_dir="$1"; 

helm lint $chart_dir

return $?
}


#--------------------------------------------------------
# comment: Create name space if not existis
# sintax:
# createNameSpace namespace
#
# return: 0 is correct or code error
#
# Example:
# createNameSpace build-helm-charts
#
# Font: https://github.com/eldada/jenkins-pipeline-kubernetes/blob/master/Jenkinsfile
function createNameSpace() {

local namespace="$1"; 

echo "Creating namespace $namespace if not existis."

[ ! -z $(kubectl get ns $namespace -o name 2> /dev/null) ] || kubectl create ns $namespace

return $?
}



#--------------------------------------------------------
# comment: Check semantic version
# sintax:
# checkSemanticVersion string
#
# return: 0 is correct or code error
#
# Example:
# checkSemanticVersion "1.2.3"
#
# Font: https://stackoverflow.com/questions/24318927/bash-regex-to-match-semantic-version-number
function checkSemanticVersion() {

local version="$1"; 

SEMANTIC_VERSION_REGEX="^(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)(\\-[0-9A-Za-z-]+(\\.[0-9A-Za-z-]+)*)?(\\+[0-9A-Za-z-]+(\\.[0-9A-Za-z-]+)*)?$"

if [[ ! "$version" =~ $SEMANTIC_VERSION_REGEX ]]; then
    echo "[ERROR] The string $version isn't compatible with semantic version: https://semver.org"
    return 1
fi

}


#--------------------------------------------------------
# comment: Check if existis command
# sintax:
# checkCommand command1 command2 command3
#
# return: 0 is correct or code error
#
# Example:
# checkCommand git kubectl helm
function checkCommand() {

local commands="$@"

for command in $commands ; do
    if ! type $command > /dev/null 2>&1; then 
        echo "[ERROR] Command '$command' not found."
        exit 4
    fi
done

}

#--------------------------------------------------------
# comment: Check if a variable is empty
# sintax:
# checkVariable name value
#
# return: 0 is correct or code error
#
# Example:
# checkVariable "variable1" "value1"
function checkVariable() {

local variable_name="$1"; 
local value="$2"; 

if [ -z $value ] ; then 
    echo "[ERROR] The variable $variable_name is empty."
    # The function usage must create in script main
    usage
    return 1
fi

}
