output "identity_pool_id" {
  description = "Cognito Identity Pool ID."
  value       = aws_cognito_identity_pool.user_pool_details.id
}
