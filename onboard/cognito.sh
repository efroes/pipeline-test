#!/bin/bash
set -e

CUSTOMER_ID="$1"
CUSTOMER_ALIAS="$2"

# Create UserPool

INPUT_JSON=$(cat <<-EOF
{
    "Policies": {
        "PasswordPolicy": {
            "MinimumLength": 10,
            "RequireUppercase": true,
            "RequireLowercase": true,
            "RequireNumbers": true,
            "RequireSymbols": true,
            "TemporaryPasswordValidityDays": 7
        }
    },
    "LambdaConfig": {
        "PostConfirmation": "${LAMBDA_ARN_THIRD_PARTY_SIGNUP}"
    },
    "Schema": [
        {
            "Name": "customerId",
            "AttributeDataType": "String",
            "DeveloperOnlyAttribute": false,
            "Mutable": true,
            "Required": false,
            "StringAttributeConstraints": {
                "MinLength": "1",
                "MaxLength": "256"
            }
        },
        {
            "Name": "userId",
            "AttributeDataType": "String",
            "DeveloperOnlyAttribute": false,
            "Mutable": true,
            "Required": false,
            "StringAttributeConstraints": {
                "MinLength": "1",
                "MaxLength": "256"
            }
        }
    ],
    "AliasAttributes": [
        "email"
    ],
    "EmailConfiguration": {
        "SourceArn": "${EMAIL_ARN}",
        "EmailSendingAccount": "DEVELOPER",
        "From": "noreply@sensedia.com"
    },
    "AdminCreateUserConfig": {
        "AllowAdminCreateUserOnly": true
    },
    "UserPoolTags": {},
    "UsernameConfiguration": {
        "CaseSensitive": false
    },
    "AccountRecoverySetting": {
        "RecoveryMechanisms": [
            {
                "Priority": 1,
                "Name": "verified_email"
            }
        ]
    }
}
EOF
)

echo Creating user pool
USER_POOL_OUTPUT=$(aws cognito-idp create-user-pool \
    --pool-name="sensedia-${CUSTOMER_ID}" \
    --cli-input-json="$INPUT_JSON")

echo $USER_POOL_OUTPUT > user_pool_output.json

export USER_POOL_ID="$(echo $USER_POOL_OUTPUT | jq -r '.UserPool.Id')"

echo "UserPool ${USER_POOL_ID} has been created."


MFA_CONFIG_INPUT_JSON=$(cat <<-EOF
{
    "SoftwareTokenMfaConfiguration": {
        "Enabled": true
    },
    "MfaConfiguration": "OPTIONAL"
}
EOF
)

aws cognito-idp set-user-pool-mfa-config \
    --user-pool-id="${USER_POOL_ID}" \
    --cli-input-json="$MFA_CONFIG_INPUT_JSON"

echo "MFA config for UserPool ${USER_POOL_ID} has been configured."

# Create User Pool Domain
aws cognito-idp create-user-pool-domain \
    --domain="sensedia-${CUSTOMER_ID}" \
    --user-pool-id="${USER_POOL_ID}"

echo "DNS for UserPool ${USER_POOL_ID} has been configured."

USER_POOL_CLIENT_INPUT=$(cat <<-EOF
{
    "GenerateSecret": false,
    "RefreshTokenValidity": 30,
    "AccessTokenValidity": 5,
    "IdTokenValidity": 5,
    "TokenValidityUnits": {
        "AccessToken": "minutes",
        "IdToken": "minutes",
        "RefreshToken": "days"
    },
    "ReadAttributes": [
        "email",
        "name",
        "custom:customerId",
        "custom:userId"
    ],
    "WriteAttributes": [
        "email",
        "name",
        "custom:customerId",
        "custom:userId"
    ],
    "ExplicitAuthFlows": [
        "ALLOW_REFRESH_TOKEN_AUTH",
        "ALLOW_USER_PASSWORD_AUTH",
        "ALLOW_CUSTOM_AUTH",
        "ALLOW_USER_SRP_AUTH"
    ],
    "SupportedIdentityProviders": [
        "COGNITO"
    ],
    "CallbackURLs": [
        "https://${CUSTOMER_ALIAS}.signin.platform-testing.sensedia-eng.com/auth/authenticated"
    ],
    "LogoutURLs": [
        "https://${CUSTOMER_ALIAS}.signin.platform-testing.sensedia-eng.com/auth/signout"
    ],
    "DefaultRedirectURI": "https://${CUSTOMER_ALIAS}.signin.platform-testing.sensedia-eng.com/auth/authenticated",
    "AllowedOAuthFlows": [
        "code"
    ],
    "AllowedOAuthScopes": [
        "email", "openid", "aws.cognito.signin.user.admin", "profile"
    ],
    "AllowedOAuthFlowsUserPoolClient": true,
    "PreventUserExistenceErrors": "ENABLED"
}
EOF
)

# Create User Pool Client
USER_POOL_CLIENT_OUTPUT=$(aws cognito-idp create-user-pool-client \
    --user-pool-id="${USER_POOL_ID}" \
    --client-name="sensedia-default" \
    --cli-input-json="$USER_POOL_CLIENT_INPUT")

echo $USER_POOL_CLIENT_OUTPUT > user_pool_client_output.json

USER_POOL_CLIENT_ID="$(echo $USER_POOL_CLIENT_OUTPUT | jq -r '.UserPoolClient.ClientId')"

echo "ClientId ${USER_POOL_CLIENT_ID} has been created."


