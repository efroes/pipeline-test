profile     = "developers-dev"
region      = "us-east-1"

environment = "testing"

tags = {
  scost = "testing"
  app = "users"
}