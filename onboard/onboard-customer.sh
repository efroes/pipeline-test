#!/bin/bash
set -e

bash check-commands.sh aws jq curl

CUSTOMER_ID="$1"
CUSTOMER_ALIAS="$2"
ENVIRONMENT="$3"

echo "Customer ID: $CUSTOMER_ID"
echo "Customer alias: $CUSTOMER_ALIAS"
echo "Environment: $ENVIRONMENT"

CONFIRMATION="true"
for arg in "$@"
do
    if [ $arg = "-y" ];  then
        CONFIRMATION="false"
    fi
done

if [ $CONFIRMATION = "true" ]; then
   read -p "Are you sure (y/n)? " -n 1 -r
   echo
   if [[ ! $REPLY =~ ^[Yy]$ ]]
   then
       [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
   fi
fi

if [ ! -f "$ENVIRONMENT-env.sh" ]; then
    echo "$ENVIRONMENT-env.sh file does not exist.";
    exit 1;
fi

source $ENVIRONMENT-env.sh

bash check-args.sh $CUSTOMER_ALIAS --customer-alias

bash cognito.sh $CUSTOMER_ID $CUSTOMER_ALIAS

bash dynamo.sh $CUSTOMER_ID $CUSTOMER_ALIAS

bash create-customer.sh $CUSTOMER_ID $CUSTOMER_ALIAS $USER_POOL_ID $USER_POOL_CLIENT_ID