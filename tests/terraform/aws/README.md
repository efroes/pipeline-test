# Sensedia Auth - Terraform

## TL;DR

```bash
# Testing Environment
terraform init
terraform workspace select testing || terraform workspace new testing
terraform plan -var-file=testing.tfvars
terraform apply -var-file=testing.tfvars
```
