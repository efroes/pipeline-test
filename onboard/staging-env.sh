#!/bin/bash

export EMAIL_ARN="arn:aws:ses:us-east-1:660412058498:identity/noreply@sensedia.com"
export LAMBDA_ARN_THIRD_PARTY_SIGNUP="arn:aws:lambda:us-east-1:660412058498:function:lambda-third-party-signup-staging"
export USER_MGMT_ALB="http://a8090259-users-usermanagem-579e-1153203844.us-east-1.elb.amazonaws.com"
export CUSTOMER_CONFIG_TABLE="CustomerConfigStaging"