#!/usr/bin/env sh

APP=iam-audit
helm delete $APP -n users
helm upgrade --install $APP . -n users
