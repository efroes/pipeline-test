resource "aws_iam_policy" "sensedia_testing_policy" {
  name        = var.cognito_public_info_policy_name
  path        = "/"
  description = "Policy required to perform automated tests"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "cognito-idp:DescribeUserPool",
                "cognito-idp:ListIdentityProviders",
                "cognito-idp:DescribeUserPoolClient",
                "cognito-idp:DeleteUserPool",
                "cognito-idp:ListUserPools",
                "cognito-idp:AdminDeleteUser",
                "cognito-idp:AdminSetUserPassword",
                "cognito-idp:ListUsers"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_cognito_identity_pool" "user_pool_details" {
  identity_pool_name               = var.cognito_public_info_identity_pool_name
  allow_unauthenticated_identities = true
  tags = var.tags
}

resource "aws_iam_role" "sensedia_testing_role" {
  name        = var.cognito_public_info_role_name
  path        = "/"
  description = "Role required to get Cognito User Pool details"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17", 
    "Statement": [
        {
            "Action": "sts:AssumeRoleWithWebIdentity", 
            "Effect": "Allow", 
            "Condition": {
                "StringEquals": {
                    "cognito-identity.amazonaws.com:aud": "${aws_cognito_identity_pool.user_pool_details.id}"
                }
            }, 
            "Principal": {
                "Federated": "cognito-identity.amazonaws.com"
            }
        }
    ]
}
EOF
  tags = var.tags
}

resource "aws_iam_role_policy_attachment" "test_attach" {
  role       = aws_iam_role.sensedia_testing_role.name
  policy_arn = aws_iam_policy.sensedia_testing_policy.arn
}

resource "aws_cognito_identity_pool_roles_attachment" "main" {
  identity_pool_id = aws_cognito_identity_pool.user_pool_details.id

  roles = {
    "authenticated"   = aws_iam_role.sensedia_testing_role.arn
    "unauthenticated" = aws_iam_role.sensedia_testing_role.arn
  }
}