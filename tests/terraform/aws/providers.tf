terraform {
  required_providers {
    aws = {
      source                  = "hashicorp/aws"
      version                 = ">= 2.7.0"
    }
  }
  required_version = ">= 0.12"
}

provider "aws" {
  shared_credentials_file = var.credentials_file
  profile                 = var.profile
  region                  = var.region
}
